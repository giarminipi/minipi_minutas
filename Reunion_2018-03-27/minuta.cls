% Copyright note: This package defines how titles should
% be typeset at the columbidae University
% Please check for updates


\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{minuta}[2015/11/18 v.01 a general package]
\LoadClass{article}

%\RequirePackage[main=spanish,english]{babel} 	% Paquetes de idioma y codificación


%===========================================
% New Commands
%===========================================
\newcommand*{\adressto}[1]{\gdef\@project{#1}}

%\newcommand*{\revision}[1]{\gdef\@revision{#1}}
%\newcommand*{\@revision}[1]{#1}

%
%\newcommand*{\@project}{Final Year Project}
%
%\newcommand*{\supervisor}[1]{\gdef\@supervisor{#1}}
%
%\newcommand*{\@supervisor}{\texttt{\string\supervisor} currently not set. Please fix this.}

%\newcommand{\headlinecolor}{\normalcolor}

\newcommand{\blankpage} {%
	\thispagestyle{empty}%
	{
		\vspace*{\fill}
		\begin{center}
			This page intentionally left blank.
		\end{center}
		\vspace*{\fill}
		\vspace*{\fill}
	} %
	\newpage
}


%===========================================
% Colores
%===========================================
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{paralist}
\RequirePackage{enumerate} %% to check paralist doesn't clash

\setdefaultleftmargin{5em}{}{}{}{}{}

%===========================================
% Hyper-References
%===========================================
\RequirePackage{hyperref}
\hypersetup{unicode=true,pdffitwindow=true,colorlinks=true,linkcolor=blue}


%===========================================
% Geometry
%===========================================
\RequirePackage[includehead, includefoot, top=1cm, headheight=2cm, headsep=1cm,%
				bottom=1cm, footskip=30pt, left=3cm, right=2cm]%
				{geometry}


%===========================================
% Revision History
%===========================================
\RequirePackage[tablegrid,owncaptions]{vhistory}

\renewcommand{\vhhistoryname}{Revisi\'on Hist\'orica}
\renewcommand{\vhversionname}{Revisi\'on}
\renewcommand{\vhdatename}	 {Fecha}
\renewcommand{\vhauthorname} {Autores}
\renewcommand{\vhchangename} {Descripci\'on}

\newcommand{\group}[1]{\def\@group{#1}}


%===========================================
% Header and Footer Definition
%===========================================
\RequirePackage{fancyhdr}	% Paquete para configurar el encabezado y pie de página
\RequirePackage{tabu}
\RequirePackage{multirow}
\RequirePackage{multicol}
\RequirePackage{graphicx}

\fancyhead{}
\fancyhead{%
	\tabulinesep=5pt
	\begin{tabu} to \linewidth [c] {|X[1.5,l]|X[5,l]|X[1,l]|X[1.3,l]|X[2,c]|}
		\hline
		T\'itulo & \multicolumn{3}{l|}{\@title}  & \multirow{3}{*}{\includegraphics[width=2.5cm]{img/GIAR}} \\ \cline{1-4}
		Grupo &	\@group	& Fecha    &	\@date	&                   \\ \cline{1-4}
		Autor  &	\@author		& Revisi\'on &	\vhCurrentVersion	&                   \\ \hline
	\end{tabu}
}
\renewcommand{\headrule}{}


\fancyfoot{} % clear all footer fields
\rfoot{\bf\thepage}
\renewcommand{\footrulewidth}{0.4pt}

\pagestyle{fancy}



%===========================================
% Chapter - Section - Subsection
%===========================================
%\renewcommand*\thesection{\arabic{section}}
\RequirePackage[uppercase, sf, explicit, raggedright]{titlesec}
\titleformat{\section}{\color{RoyalBlue}\sf\large\bf}{\thesection\hspace{2pt}}{.5em}{#1}
\titleformat{\subsection}{\color{RoyalBlue}\sf\large\bf}{\thesubsection\hspace{2pt}}{.5em}{#1}
\titleformat{\subsubsection}{\sf\normalsize\bf}{\thesubsubsection\hspace{2pt}}{.5em}{#1}

\setcounter{secnumdepth}{3}



%===========================================
% Title Page
%===========================================

\renewcommand{\maketitle}{\include{titlepage}}


