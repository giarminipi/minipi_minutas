﻿# Minutas del grupo **Robótica Colaborativa** #

### ¿Para que es este repositorio?  ###

* En este respositorio se irán subiendo todas las minutas de las reuniones efectuadas por el grupo **Robótica Colaborativa**.

### ¿Cómo subir una nueva minuta? ###

* Copiar la carpeta "Template" y modificarle el nombre a "Reunion_<año>_<mes>_<día>". Luego hacer las modificaciones necesarias al documento.
* Finalmente compilar y dejar disponible un archivo PDF.

### ¿Qué hago si quiero hacer cambios en una minuta? ###

* Agregar en el archivo "Reunion.tex" del proyecto otra línea del siguiente tipo: \vhEntry{<versión>}{<día>/<mes>/<año>}{<autor>}{<comentario>}.